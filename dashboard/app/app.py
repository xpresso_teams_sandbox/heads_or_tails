__author__ = "heads_or_tails"


import json
import os
import logging
from json import JSONDecodeError
from flask import Flask
from flask import request
from xpresso.ai.core.logging.xpr_log import XprLogger


config_file = 'config/dev.json'

logger = XprLogger("dashboard",level=logging.INFO)

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_table

import pandas as pd
import plotly.graph_objs as go

files=os.listdir('/inf_data/xp_aa_hack_submissions')
files.remove('heads_predictions.csv')
files.remove('modified_train.csv')
files.remove('predictions.csv')
filename = sorted(files)[-1]
filename = os.path.join('/inf_data/xp_aa_hack_submissions',filename)
predictions = pd.read_csv(os.path.join(filename,'predictions.csv'))
train = pd.read_excel('/inf_data/dashboard_data/train.xlsx') 
train_mod = pd.read_csv('/inf_data/xp_aa_hack_submissions/modified_train.csv')

train_mod = train_mod.sort_values(by = ['Country', 'Date'], ascending = True)
train_mod['confirmed'] = train_mod.groupby(['Country'])['new_cases'].apply(lambda x: x.cumsum())
countries = [{'label' : cnt, 'value' : cnt} for cnt in train_mod['Country'].unique().tolist()]

sum_cases = pd.DataFrame(predictions.groupby('Country')['Confirmed'].sum()).reset_index()
sum_cases = sum_cases.sort_values('Confirmed', ascending=False)


app = dash.Dash(__name__, external_stylesheets = [dbc.themes.BOOTSTRAP])
server = app.server
app.config.suppress_callback_exceptions = True

#set the app.layout
app.layout = html.Div([
  html.Div([
    html.H1('Country Wise Confirmed Cases'),
    dcc.Dropdown(
        id='country-dropdown',
        options=countries,
        multi=False,
        #value = countries[0]['value']
        value = 'Italy'
    ),
    dcc.Graph(id='country_confirmed'),
    html.H1('Country Wise New Cases'),
    dcc.Graph(id='country_new_cases'),
    html.H1('Provinces'),
    dcc.Graph(id='country_province'),
    html.H1('India'),
    dcc.Graph(id='country_peak'),
    html.H1('Countries with Highest Cases in coming 14 days'),
    dcc.Graph(id='highest_cases'),
    html.H1('Country wise growth'),
    dcc.Graph(id='growth')
  ], style={'width': '90%', 'display': 'inline-block'})
])

@app.callback(Output('country_confirmed', 'figure'), [Input('country-dropdown', 'value')])
def confirmed(X):
  temp_df = train_mod[train_mod['Country'] == X]
  trace_1 = go.Scatter(x = temp_df['Date'], y = temp_df['confirmed'],
            name = X,
            line = dict(width = 2,
                  color = 'rgb(106, 181, 135)'))
  layout = go.Layout(title = 'Confirmed')
  fig = go.Figure(data = [trace_1], layout = layout)
  return fig

@app.callback(Output('country_new_cases', 'figure'), [Input('country-dropdown', 'value')])
def new_cases(X):
  temp_df = train_mod[train_mod['Country'] == X]
  trace_2 = go.Bar(x = temp_df['Date'], y = temp_df['new_cases'],
          name = X, text=temp_df['new_cases'], textposition='auto',)
  layout = go.Layout(title = 'New Cases')
  fig = go.Figure(data = [trace_2], layout = layout)
  return fig

@app.callback(Output('country_province', 'figure'), [Input('country-dropdown', 'value')])
def country_province(X):
  temp_df = train[(train['Country'] == X)&(train['Date']==train['Date'].max())]
  trace_3 = go.Bar(x = temp_df['Province'], y = temp_df['Confirmed'],
          name = X, text = temp_df['Confirmed'], textposition='auto')
  layout = go.Layout(title = 'Province Confirmed Cases')
  fig = go.Figure(data = [trace_3], layout = layout)
  return fig

@app.callback(Output('country_peak', 'figure'), [Input('country-dropdown', 'value')])
def country_peak(X):
  temp_df = train_mod[train_mod['Country'] == 'India']
  trace_4 = go.Bar(x = temp_df['Date'], y = temp_df['new_cases'],
          name = 'India', text = temp_df['new_cases'], textposition='auto')
  layout = go.Layout(title = 'India')
  fig = go.Figure(data = [trace_4], layout = layout)
  return fig

@app.callback(Output('highest_cases', 'figure'), [Input('country-dropdown', 'value')])
def highest_cases(X):
  temp_df = sum_cases.head(10).sort_values('Confirmed')
  trace_1 = go.Bar(x = temp_df['Confirmed'], y = temp_df['Country'],
                 orientation='h')
            # name = X,
            # line = dict(width = 2,
                  # color = 'rgb(106, 181, 135)'))
  layout = go.Layout(title = 'Confirmed')
  fig = go.Figure(data = [trace_1], layout = layout)
  return fig


@app.callback(Output('growth', 'figure'), [Input('country-dropdown', 'value')])
def growth_country(X):
  temp_df = predictions[predictions['Country'] == X]
  trace_1 = go.Scatter(x = temp_df['Date'], y = temp_df['Confirmed'],
            name = X,
            line = dict(width = 2,
                  color = 'rgb(106, 181, 135)'))
  layout = go.Layout(title = 'Confirmed')
  fig = go.Figure(data = [trace_1], layout = layout)
  return fig



if __name__ == "__main__":
    app.run_server('0.0.0.0', 8080,debug = True)    

