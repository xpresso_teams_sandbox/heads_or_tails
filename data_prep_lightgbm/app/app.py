"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Sujoy De"

logger = XprLogger("data_prep_lightgbm",level=logging.INFO)


class DataPrepLightgbm(AbstractPipelineComponent):
    """
    For complete preparation of data for training
    """

    def __init__(self, train_file_path, test_file_path, country_file_path):
        """ Read data from csv file into dataset"""
        super().__init__(name="DataPrepLightgbm")
        self.train_data = pd.read_csv(train_file_path, parse_dates=['Date'])
        self.country_data = pd.read_csv(country_file_path)
        self.test_data = None
        self.look_back = 7

    @staticmethod
    def keep_only_country(train):
        '''
        Remove Diamond Princess and MS Zaandam from the data
        '''
        train = train[~train['Country'].isin(['Diamond Princess', 'MS Zaandam'])]
        return train

    @staticmethod
    def group_by_country(train):
        '''
        Remove the province and group the data values based on country
        '''
        train = train.groupby(['Country', 'Date'])['Confirmed'].sum().reset_index()
        train.sort_values(by=['Country', 'Date'], ascending=True, inplace=True)
        return train

    @staticmethod
    def get_previous_cases(train):
        '''
        Get previous day cases for the data
        '''
        train['previous_cases'] = train.groupby(['Country'])['Confirmed'].transform(
            lambda x: x.shift(1, fill_value=np.nan))
        return train

    @staticmethod
    def get_new_cases(row):
        '''
        Get new cases by subtracting previous cases from confirmed cases
        '''
        if row['previous_cases'] != np.nan:
            new_cases = max(row['Confirmed'] - row['previous_cases'], 0)
        else:
            new_cases = np.nan
        return new_cases

    @staticmethod
    def get_first_day(row, train):
        '''
        Get date for a country when the first infection was reported
        '''
        temp_df = train[(train['Country'] == x) & (train['new_cases'] > 0)]
        if temp_df.shape[0]>=1:
            first_date = temp_df['Date'].min()
        else:
            first_date = temp_df['Date'].max()
        return first_date

    @staticmethod
    def get_day_of_infection(row):
        '''
        Get days since first infection was reported
        '''
        first_date = row['date_first_infection']
        current_date = row['Date']
        progress_infection = (current_date - first_date).days
        progress_infection = max(progress_infection, 0)
        return progress_infection

    @staticmethod
    def get_prior_feats(time_step, train):
        '''
        Get new cases of prior days for look back period
        '''
        col = 'prior_day_' + str(time_step)
        train[col] = train.groupby(['Country'])['new_cases'].transform(
            lambda x: x.shift(time_step, fill_value=np.nan))
        return train

    @staticmethod
    def calculate_cases_characteristics(row, prior_cols):
        '''
        Get statistical characteristics of new cases for look back period
        '''
        prior_values = [row[col] for col in prior_cols]
        mean_cases = np.mean(prior_values)
        median_cases = np.median(prior_values)
        std_cases = np.std(prior_values)
        skew_cases = (median_cases - mean_cases) / median_cases if median_cases != 0 else 0
        return pd.Series([mean_cases, median_cases, std_cases, skew_cases])

    @staticmethod
    def calculate_trend(row, time_step):
        '''
        Calculate trend for the look back period {trend = (x(t-1) - x(t-n))/x(t-n)}
        '''
        col = 'prior_day_' + str(time_step)
        if row[col] == np.nan or row[col] == 0:
            trend = row['prior_day_1']
        else:
            trend = (row['prior_day_1'] - row[col]) / x[col]
        return trend

    @staticmethod
    def merge_country_details(train, country_data):
        '''
        Merge train with country data
        '''
        train = pd.merge(train, country_data, on = 'Country', how = 'left')
        return train

    def start(self, run_name):
        """
        1. Remove Diamond Princess and MS Zaandam from the data
        2. Remove the province and group the data values based on country
        3. Get new cases by subtracting previous cases from confirmed cases
        4. Get new cases of prior days for look back period
        5. Get statistical characteristics of new cases for look back period
        6. Calculate trend for the look back period {trend = (x(t-1) - x(t-n))/x(t-n)}
        7. Merge train with country data
        """
        super().start(xpresso_run_name=run_name)
        logging.info("Preparing data")
        self.train = self.keep_only_country(self.train)
        self.train = self.group_by_country(self.train)
        self.train = self.get_previous_cases(self.train)

        self.train['new_cases'] = self.train.apply(lambda x : self.get_new_cases(x), axis = 1)
        self.send_metrics("Calculated new cases", self.train)

        self.train.drop(['Confirmed', 'previous_cases'], axis=1, inplace=True)
        self.send_metrics("Dropping confirmed and previous cases", self.train)

        self.train['date_first_infection'] = self.train.apply(lambda x : self.get_first_day(x, self.train), axis = 1)
        self.train['progress_infection'] = self.train.apply(lambda x: self.get_day_of_infection(x), axis=1)
        for i in range(look_back):
            self.train = self.get_prior_feats(time_step = i + 1, train = self.train)
        self.send_metrics("Calculated prior day values", self.train)

        prior_cols = [col for col in self.train.columns if 'prior' in col]
        self.train.dropna(how='any', subset=prior_cols, axis=0, inplace=True)
        self.send_metrics("Dropped rows with nan values because of look back operation", self.train)

        cases_cols = ['prior_mean_cases', 'prior_median_cases', 'prior_std_cases', 'prior_skew_cases']
        self.train[cases_cols] = self.train.apply(lambda x: self.calculate_cases_characteristics(x, cases_cols), axis=1)
        self.send_metrics("Added columns for statistical characteristics of prior day columns", self.train)

        for i in range(1, look_back):
            col = 'trend_' + str(i + 1)
            self.train[col] = self.train.apply(lambda x: self.calculate_trend(x, i + 1), axis=1)
        self.send_metrics("Calculated trend", self.train)

        self.train = self.merge_country_details(train = self.train, country_data=self.country_data)
        self.send_metrics("Merged train and country details Data", self.train)
        self.completed()

    def send_metrics(self, status, metric_data):
        '''
        Send metrics to xpresso controller which can be retrieved or visualized later
        '''
        report_status = {"status": {"status": status},
                         "metric": {
                             "rows": str(metric_data.shape[0]),
                             "columns": str(metric_data.shape[1])}
                         }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        '''
        Data processing is completed. Saving the output in a file and
        persisting the state
        '''
        output_dir = "/data/"
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        self.train.to_csv(os.path.join(output_dir,"lgb_train.csv"),
                          index=False)
        logging.info("Data saved")
        super().completed(push_exp=push_exp)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = DataPrepLightgbm(
        train_file_path="/data/train.csv",
        test_file_path="/data/test.csv",
        store_file_path="/data/store.csv")
    data_prep.start(run_name=sys.argv[1])
    '''
    data_prep = DataPrepLightgbm()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
    '''
